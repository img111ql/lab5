
//change background color of TD containing variables
var myTable = document.getElementsByTagName('table');
console.log("Number of rows: " + myTable[0].rows.length);
console.log("Number of columns: " + myTable[0].rows[0].cells.length);

for (var line = 0; line < myTable[0].rows.length; line++) {
    var myRow = myTable[0].rows[line];
    myRow.style.border = "4px solid green";
    // write a loop to access each cell, i.e. ID element
    for (var column = 0; column < myRow.cells.length; column++) {
        var myCell = myRow.cells[column];
        if (myCell.innerHTML === "Paris")
        {
            myCell.style.backgroundColor = "red";
        }
    }
}

// write a function that will make all images circular
function makeAllImagesCircular() {
    var images = document.getElementsByTagName('img');
    console.log(images.length);
    for (var index = 0; index < images.length; index++) {
        var myImage = images[index];
        myImage.className += "rounded-circle";
    }
}
makeAllImagesCircular();


//write a function that changes font size to 15px for all paragraphs

function changeParagraphFontSize(size) {

    var pars = document.getElementsByTagName('p');
    console.log(pars.length);

    for (var counter = 0; counter < pars.length; counter++) {
        //pars[counter].style.border = "5px solid lightblue";
        pars[counter].style.fontSize = size;
    }
}

changeParagraphFontSize("1.4em");

// modify the table so that each TD element has a different background color



//

var myTable = document.getElementsByTagName('table');
//console.log(myTable);
console.log("Number of rows: " + myTable[0].rows.length);
console.log("Number of columns: " + myTable[0].rows[0].cells.length);

// write a loop for each table row
for (var line = 0; line < myTable[0].rows.length; line++) {
    var myRow = myTable[0].rows[line];
    myRow.style.border = "4px solid green";
    // write a loop to access each cell, i.e. ID element
    for (var column = 0; column < myRow.cells.length; column++) {
        var myCell = myRow.cells[column];
        console.log(myCell.innerHTML);


        myCell.style.border = "3px solid red";
        myCell.innerHTML = "" + line + " " + column;
        myCell.style.backgroundColor = "rgba(1, 2, 3, 0.5)";
        myCell.style.backgroundColor = 'rgba(' + line * 1000 + "," + line * 100 + "," + column * 10 + ',0.5)';
    }
}

myTable[0].classList = "table table-bordered table-striped";

//

var myuls = document.getElementsByTagName('ul');
console.log(myuls);

//myuls[0].style.border = "5px solid green";
myuls[1].style.border = "5px solid green";

// append new li to the UL with text "we did it"

// 1. create LI node
var myli = document.createElement('LI');
myli.innerHTML = "We did it!";

// 2. Append the created element to the UL node
myuls[1].appendChild(myli);

// as design mistake append a paragraph to the ul
// var myp = document.createElement("p");
// myp.innerHTML = "Do not do this!";
// myuls[1].appendChild(myp);